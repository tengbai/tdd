import HelloWold.HelloWorld;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.BDDAssertions.then;


public class HelloWorldTest {


    @Test
    void should_get_hello_word_string_when_say_hello_given_a_hello_world_instance() {
        HelloWorld helloWorld = new HelloWorld();

        String result = helloWorld.sayHello();

        then(result).isEqualTo("hello world");
    }
}
